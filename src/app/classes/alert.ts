import { AlertController, ToastController, IonicModule, ModalController } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// export this for use in other components
@NgModule({
    imports: [
        CommonModule,
        IonicModule,
    ],
    declarations: []
})

export class Notification  {
    constructor(
        public alertController: AlertController,
        public toastController: ToastController,
        public modalController: ModalController) {}

    async presentAlert(message, header) {
        const alert = await this.alertController.create({
            header: `${header}`,
            message: `${message}`,
            buttons: ['OK']
        });

        await alert.present();
    }

    async presentToast(message) {
        const toast = await this.toastController.create({
            message: `${message}`,
            duration: 2000
        });
        toast.present();
    }

    async presentModal(page) {
        const modal = await this.modalController.create({
        component: page,
        componentProps: { value: 123 }
        });

        await modal.present();
    }
}
