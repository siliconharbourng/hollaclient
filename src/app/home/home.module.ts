import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { MapComponent } from '../components/map/map.component';
import { PickupComponent } from '../components/pickup/pickup.component';
import { AvailableWorkersComponent } from '../components/available-workers/available-workers.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, MapComponent, PickupComponent, AvailableWorkersComponent],
  entryComponents: [],
})
export class HomePageModule {}
