import { Component, OnChanges, SimpleChanges, Input, OnInit } from '@angular/core';
import { TokenService } from '../services/token.service';
import io from 'socket.io-client';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { environment } from 'src/environments/environment';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { BookingService } from '../services/booking.service';
import { PendingPage } from '../modal/pending/pending.page';
import { ModalController, NavController } from '@ionic/angular';
import { Notification } from '../classes/alert';

const SOCKET = environment.socket;


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  location;
  socket: any;
  id;
  num;
  skill;
  value;
  status;
  public requestedWorker: boolean;
  public request: boolean;
  public confirm: boolean;
  public accepted: boolean;
  public arrived: boolean;
  public jobinprogress: boolean;

  public acceptedWorker = {
    first_name: '',
    last_name: '',
    phone: '',
    message: '',
    slug: ''
  };

  public cancel = {
    status: 'cancel',
    cancelReason: {
      reason: 'Worker didnt arrive on time'
    }
  };


  constructor(
    private token: TokenService,
    public formBuilder: FormBuilder,
    private navCtrol: NavController,
    private geolocation: Geolocation,
    private booking: BookingService,
    public modalController: ModalController,
    private notify: Notification) {
    this.socket = io(`${SOCKET}`);
    this.confirm = false;
    this.request = true;
    this.accepted = false;
    this.arrived = false;
    this.jobinprogress = false;
    this.requestedWorker = false;
  }

  ngOnInit() {
    this.getCurrentLocation().subscribe();
    this.token.Getpayload().then(data => {
      this.id = data;
      this.Availablejobs(data);
    });
    this.socket.on('refreshpage', data => {
      this.Availablejobs(this.id);
    });
    console.log(this.arrived);
  }

  onSearchChange($event) {

  }

  workprofile(slug) {
    this.navCtrol.navigateForward(`worker/${slug}`);
  }

  getCurrentLocation() {
    const Obs = Observable.create(obs => {
      this.geolocation.getCurrentPosition().then(res => {
        const lat = res.coords.latitude;
        const lng = res.coords.longitude;

        this.location = [lat, lng];
        console.log(this.location);
        obs.next(location);
      }, err => {
        console.log(err);
      });
    });
    return Obs;
  }

  onClick() {
    this.confirm = true;
    this.request = false;
    this.requestedWorker = true;
  }

  confirmation() {
    this.confirm = false;
    this.booking.requestWorker(this.skill, this.id, 'New User', { coordinates: this.location }).subscribe(data => {
      this.socket.emit('refresh', {});
    }, err => {
        if (err.error.details) {
          const msg = 'An Error occoured';
          this.notify.presentAlert(msg, 'Error');
        } else if (err.error.message) {
          this.notify.presentAlert(err.error.message, 'Error');
        }
    });
  }

  Availablejobs(id) {
    this.booking.availablejobs(id).subscribe(book => {
      try {
        this.acceptedWorker.slug = book[0].workersId.slug;
        const status = book[0].status;
        this.status = status;
        if (status === 'pending') {
          this.request = false;
          this.confirm = false;
          this.presentModal();
          return this.accepted = false;
        }

        if (status === 'accepted') {
          console.log(book[0].workersId.firstName);
          this.acceptedWorker.first_name = book[0].workersId.firstName;
          this.acceptedWorker.last_name = book[0].workersId.lastName;
          this.acceptedWorker.phone = book[0].workersId.phone;
          this.acceptedWorker.message = `${this.acceptedWorker.first_name} ${this.acceptedWorker.last_name} has accepted your offer`;
          setTimeout(() => {
          this.acceptedWorker.message = `${this.acceptedWorker.first_name} ${this.acceptedWorker.last_name} is on the way`;
          }, 5000);
          setTimeout(() => {
          this.acceptedWorker.message = `${this.acceptedWorker.first_name} ${this.acceptedWorker.last_name} is approaching`;
          }, 10000);
          this.accepted = true;
          this.request = false;
          this.confirm = false;
          this.arrived = false;
        }

        if (status === 'arrived') {
          this.acceptedWorker.first_name = book[0].workersId.firstName;
          this.acceptedWorker.last_name = book[0].workersId.lastName;
          this.acceptedWorker.last_name = book[0].workersId.lastName;
          this.acceptedWorker.phone = book[0].workersId.phone;
          this.acceptedWorker.message = `${this.acceptedWorker.first_name} ${this.acceptedWorker.last_name} has arrived your location`;
          this.accepted = true;
          this.request = false;
          this.confirm = false;
          return this.arrived = true;
        }

        if (status === 'inprogress') {
          this.acceptedWorker.first_name = book[0].workersId.firstName;
          this.acceptedWorker.last_name = book[0].workersId.lastName;
          this.acceptedWorker.phone = book[0].workersId.phone;
          this.acceptedWorker.message = `${this.acceptedWorker.first_name} ${this.acceptedWorker.last_name} currently working`;
          this.accepted = true;
          this.request = false;
          this.confirm = false;
          this.arrived = false;
        }
      } catch (error) {
        this.confirm = false;
        this.request = true;
        this.accepted = false;
        this.arrived = false;
        this.jobinprogress = false;
      }
    });
  }

  startjob() {
    this.jobinprogress = false;
    this.booking.status(this.id, { status: 'inprogress'}).subscribe(data => {
      this.socket.emit('refresh', {});
    });
  }

  canceljob() {
    this.booking.cancel(this.id, this.cancel).subscribe(data => {
      this.socket.emit('refresh', {});
      console.log(data);
    }, err => {
      console.log(err);
    });
  }

  endJOb() {
    this.booking.complete(this.id, { status: 'completed'}).subscribe(() => {
      this.socket.emit('refresh', {});
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
    component: PendingPage,
    componentProps: { value: 123 },
    cssClass: 'waiting'
    });

    await modal.present();

  }
}
