import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { TokenService } from './token.service';
import { Notification } from '../classes/alert';
import { Router } from '@angular/router';

const BASE_URL = environment.url;
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authenticationState = new BehaviorSubject(false);

  constructor(
    private http: HttpClient,
    private platform: Platform,
    private token: TokenService,
    private notify: Notification,
    private router: Router) {
      this.platform.ready().then(() => {
        this.checkToken();
        this.isAutheticated();
      });
     }

  checkToken() {
    this.token.getToken().then(token => {
      if (token) {
        return this.authenticationState.next(true);
      } else {
        return this.authenticationState.next(false);
      }
    });
  }

  register(payload) {
    const hint = 'Your Registration is successfull';
    this.http.post(`${BASE_URL}/auth/register/client`, payload).subscribe(data => {
      this.token.setToken(data[userToken()]);
      this.notify.presentToast(hint);
      this.authenticationState.next(true);
      this.router.navigateByUrl('/home');
    }, err => {
      if (err.error.details) {
        const msg = 'Make sure you Input the correct details and retry again';
        this.notify.presentAlert(msg, 'Registration Error');
      } else if (err.error.message) {
        this.notify.presentAlert(err.error.message, 'Registration Error');
      }
    });
  }

  login(payload) {
    const hint = 'Welcome back!!!, Its nice you came back';
    this.http.post(`${BASE_URL}/auth/signin`, payload).subscribe(data => {
      this.token.setToken(data[userToken()]);
      this.notify.presentToast(hint);
      this.authenticationState.next(true);
      this.router.navigateByUrl('/home');
    }, err => {
        if (err.error.details) {
          const msg = 'Make sure you Input the correct details and retry again';
          this.notify.presentAlert(msg, 'Sign In Error');
        } else if (err.error.message) {
          this.notify.presentAlert(err.error.message, 'Sign In Error');
        }
    });
  }

  isAutheticated() {
    return !!this.authenticationState.value;
  }
}



function userToken() {
  return 'token';
}

