import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private storage: Storage) { }

  getToken() {
    return this.storage.get('auth-token');
  }

  setToken(token) {
    return this.storage.set('auth-token', token);
  }

  deleteToken() {
    return this.storage.remove('auth-token');
  }

  async Getpayload() {
    const token = await this.storage.get('auth-token');
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = JSON.parse(window.atob(payload));
    }

    return payload.id;
  }
}
