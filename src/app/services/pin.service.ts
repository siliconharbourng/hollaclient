import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PinService {

  public pin: google.maps.Marker;
  constructor() { }

  showPin(mapId, location) {
    this.pin = new google.maps.Marker({
      map: mapId,
      position: location,
      icon: ''
    });

    setTimeout(() => {
      this.pin.setAnimation(null);
    }, 750);
  }

  removePin() {
    if (this.pin) {
      this.pin.setMap(null);
    }
  }
}
