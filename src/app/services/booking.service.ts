import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const BASE_URL = environment.url;
@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  requestWorker(skill, seekersId, sender, jobLocation) {
    return this.http.post(`${BASE_URL}/booking/request`, {
      skill,
      seekersId,
      sender,
      jobLocation});
  }

  availablejobs(id) {
    return this.http.get(`${BASE_URL}/booking/request/${id}`);
  }

  status(id, payload) {
    return this.http.patch(`${BASE_URL}/booking/request/${id}`, payload);
  }

  cancel(id, payload) {
    return this.http.put(`${BASE_URL}/booking/request/${id}/cancel`, payload);
  }

  complete(id, payload) {
    return this.http.put(`${BASE_URL}/booking/request/${id}/complete`, payload);
  }
}
