import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-worker-profile',
  templateUrl: './worker-profile.page.html',
  styleUrls: ['./worker-profile.page.scss'],
})
export class WorkerProfilePage implements OnInit {

  worker: any;

  constructor(private route: ActivatedRoute, private user: UserService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.user.getUser(params.slug).subscribe(data => {
        this.worker = data;
      });
    });
  }

}
