import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(private NavCtrl: NavController) { }

  ngOnInit() {
  }

  registerPage() {
    this.NavCtrl.navigateForward('/register');
  }

  loginPage() {
    this.NavCtrl.navigateForward('/login');
  }
}
