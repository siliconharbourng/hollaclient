import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public RegisterForm;

  constructor(private formbuilder: FormBuilder, private auth: AuthService) {
    this.RegisterForm = this.formbuilder.group({
      email: ['', [Validators.required, this.emailValid()]],
      phone: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      accept_discliamer: [ null, Validators.required],
    }, { validator: this.matchingField('password', 'confirmPassword') });
  }

  ngOnInit() {
  }

  onSubmit() {
  delete this.RegisterForm.value.confirmPassword;
  this.auth.register(this.RegisterForm.value);
  // console.log(this.RegisterForm.value);
  }

  isValid(control) {
    return this.RegisterForm.controls[control].invalid && this.RegisterForm.controls[control].touched;
  }

  matchingField(field1, field2) {
    return form => {
      if (form.controls[field1].value !== form.controls[field2].value) {
        return { mismatchfields: true };
      }
    };
  }

  emailValid() {
    return control => {
      // tslint:disable-next-line:max-line-length
      const rejex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return rejex.test(control.value) ? null : { invalidEmail: true };
    };
  }
}
