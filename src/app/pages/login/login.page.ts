import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginForm;

  constructor(public formBuilder: FormBuilder, private auth: AuthService) {
    this.loginForm = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required],
      rights: 'client'
    });
   }

  ngOnInit() {
  }

  onSubmit() {
    this.auth.login(this.loginForm.value);
  }

}
