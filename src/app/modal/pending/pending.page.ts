import { Component, OnInit } from '@angular/core';
import { BookingService } from 'src/app/services/booking.service';
import { TokenService } from 'src/app/services/token.service';
import { ModalController } from '@ionic/angular';
import io from 'socket.io-client';
import { environment } from 'src/environments/environment';

const SOCKET = environment.socket;
@Component({
  selector: 'app-pending',
  templateUrl: './pending.page.html',
  styleUrls: ['./pending.page.scss'],
})
export class PendingPage implements OnInit {

  public job;
  public id;
  socket: any;
  public cancel = {
    status: 'cancel',
    cancelReason: {
      reason: 'Worker didnt arrive on time'
    }
  };

  constructor(
    private book: BookingService,
    private token: TokenService,
    public modalController: ModalController) {
      this.socket = io(`${SOCKET}`);
     }

  ngOnInit() {
    this.token.Getpayload().then(data => {
      this.id = data;
      this.book.availablejobs(data).subscribe(jobs => {
        this.job = jobs[0]._id;
        if (jobs[0].status !== 'pending') {
          this.dismissModal();
        }
      });
    });
    this.socket.on('refreshpage', data => {
      if (this.job[0].status !== 'pending') {
        this.dismissModal();
      }
    });
  }

  dismissModal() {
    this.modalController.dismiss();
  }

  cancelled() {
    this.book.cancel(this.id, this.cancel).subscribe(data => {
      this.socket.emit('refresh', {});
      this.dismissModal();
      console.log(data);
    }, err => {
      console.log(err);
    });
  }
}
