import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import io from 'socket.io-client';
import { environment } from 'src/environments/environment';

const SOCKET = environment.socket;

@Component({
  selector: 'available-workers',
  templateUrl: './available-workers.component.html',
  styleUrls: ['./available-workers.component.scss'],
})
export class AvailableWorkersComponent implements OnInit, OnChanges {

  @Input() requestedWorker: boolean;
  @Input() map: google.maps.Map;

  socket: any;

  public workerMarker: Array<google.maps.Marker>;
  private Marker: google.maps.Marker;

  constructor(private worker: UserService) {
    this.workerMarker = [];
    this.socket = io(`${SOCKET}`);
  }

  ngOnInit() {
    this.fetchandrefreshWorkers();
    this.socket.on('refreshpage', data => {
      this.fetchandrefreshWorkers();
    });
  }

  ngOnChanges(changes) {
    if (this.workerMarker) {
      this.removeMarker();
    }
  }


  fetchandrefreshWorkers() {
    this.worker.getworkers().subscribe(data => {
      console.log(data['length']);
      // console.log(data[0].location[0]);
      if (!this.requestedWorker) {
      // tslint:disable-next-line:no-var-keyword
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < data['length']; i++) {
        // tslint:disable-next-line:prefer-const
        this.Marker = new google.maps.Marker({
          map: this.map,
          position: new google.maps.LatLng(
            data[i].location[0].coordinates[1],
            data[i].location[0].coordinates[0]
          ),
          icon: '../../assets/icon/worker.png'
        });

        this.workerMarker.push(this.Marker);

        // }
        // tslint:disable-next-line:prefer-for-of
        for (let b = 0; b < this.workerMarker['length']; b++) {
          if (this.workerMarker[b]) {
            this.workerMarker[b].setPosition(new google.maps.LatLng(
              data[i].location[0].coordinates[1],
              data[i].location[0].coordinates[0])
            );
          }
        }
        }
      }
    });
  }

  removeMarker() {
    if (this.Marker) {
      this.Marker.setMap(null);
    }
  }

}
