import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Observable } from 'rxjs';
import { PinService } from 'src/app/services/pin.service';
import { TokenService } from 'src/app/services/token.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {

  @Input() requestedWorker: boolean;
  public map: google.maps.Map;
  public Marker: Array<google.maps.Marker>;
  public isMapIdle: boolean;

  constructor(
    private router: Router,
    private geolocation: Geolocation,
    private pin: PinService,
    private token: TokenService,
    private user: UserService) {
    this.Marker = [];
  }

  ngOnInit() {
    this.map = this.createMap();
    this.getCurrentLocation().subscribe(location => {
      this.centerLocation(location);
    });
    this.watchPosition();
    this.addmapEventListener();
  }

  addmapEventListener() {
    google.maps.event.addListener(this.map, 'dragstart', () => {
      this.isMapIdle = false;
    });
    google.maps.event.addListener(this.map, 'idle', () => {
      this.isMapIdle = true;
    });
  }

  getCurrentLocation() {
    const Obs = Observable.create(obs => {
      this.geolocation.getCurrentPosition().then(res => {
        const lat = res.coords.latitude;
        const lng = res.coords.longitude;

        const location = new google.maps.LatLng(lat, lng);
        obs.next(location);
      }, err => {
        console.log(err);
      });
    });
    return Obs;
  }

  watchPosition() {
    this.geolocation.watchPosition().subscribe(resp => {
      const lat = resp.coords.latitude;
      const lng = resp.coords.longitude;

      this.token.Getpayload().then(data => {
          this.user.saveLocation([lng, lat], data);
      });
      // tslint:disable-next-line:prefer-const
      let location = new google.maps.Marker({
        map: this.map,
        position: new google.maps.LatLng(lat, lng),
        icon: '../../assets/icon/marker.png'
      });

      location.set('id', 123);

      this.Marker.push(location);
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.Marker.length; i++) {
        if (this.Marker[i]) {
          this.Marker[i].setPosition(new google.maps.LatLng(lat, lng));
        }
      }
    }, err => {
      console.log(err);
    });
  }

  createMap() {
    try {
      const mapOptions = {
        center: { lat: 6.439292, lng: 3.471133},
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
      };

      const mapEl = document.getElementById('map');
      const map = new google.maps.Map(mapEl, mapOptions);

      return map;
    } catch (error) {
      this.router.navigate(['/error404']);
    }
  }

  centerLocation(location) {
    if (location) {
      this.map.panTo(location);
    } else {
      this.getCurrentLocation().subscribe(currentLocation => {
        this.map.panTo(currentLocation);
      });
    }
  }

}
