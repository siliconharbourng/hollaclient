import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'pickup',
  templateUrl: './pickup.component.html',
  styleUrls: ['./pickup.component.scss'],
})
export class PickupComponent implements OnChanges {

  @Input() isPinSet: boolean;
  @Input() map: google.maps.Map;

  private pickupMarker: google.maps.Marker;

  constructor() { }

  ngOnChanges() {
    if (this.isPinSet) {
      this.showPickupMarker();
    } else {
      this.removeMarker();
    }
  }

  showPickupMarker() {
    this.pickupMarker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.BOUNCE,
      position: this.map.getCenter(),
      icon: '../../assets/icon/pin.png'
    });

    setTimeout(() => {
      this.pickupMarker.setAnimation(null);
    }, 750);
  }

  removeMarker() {
    if (this.pickupMarker) {
      this.pickupMarker.setMap(null);
    }
  }

}
