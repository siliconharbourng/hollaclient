export const environment = {
         production: true,
         url: 'https://hollatest.herokuapp.com/api/v1',
         socket: 'https://hollatest.herokuapp.com'
       };
